package quantum.kpfu.ru.kotlinclient

import io.reactivex.schedulers.Schedulers
import quantum.kpfu.ru.kotlinclient.operators.singlequbitoperator.Hadamar
import quantum.kpfu.ru.kotlinclient.operators.singlequbitoperator.S

fun main(args: Array<String>) {
    val n = 4
    QuantumProgram.Builder()
            .setCountQubits(n)
            .setOperators(
                    *Hadamar.use(0 until n)
//                    S(1)
            )
            .build()
            .subscribeOn(Schedulers.newThread())
            .observeOn(Schedulers.newThread())
            .subscribe(
                    {
                        println(it)
                    },
                    {
                        it.printStackTrace()
                    }
            )

    readLine()

}

//inline fun <T, reified R> Iterable<T>.perform(transform: (T) -> R): Array<R> {
//    return this.map {
//        transform(it)
//    }.toTypedArray()
//}
//
//inline fun <reified T : AbstractOperator, reified R> AbstractOperator.perform(transform: () -> List<Int>) {
//
//    transform().map {
//        val classRef = T::class
//        classRef.primaryConstructor?.parameters?.forEach {
//            println(it)
//        }
//    }
//}
