package quantum.kpfu.ru.kotlinclient.rest

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object Rest {
    val BASE_URL = "http://localhost:8080/v1/qvm/"

    lateinit var api: QuantumPlatformApi
    lateinit var retrofit: Retrofit
    init {
        api = getRetrofitInstance().create(QuantumPlatformApi::class.java)
    }


    fun getRetrofitInstance(): Retrofit {
        retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        return retrofit
    }
}