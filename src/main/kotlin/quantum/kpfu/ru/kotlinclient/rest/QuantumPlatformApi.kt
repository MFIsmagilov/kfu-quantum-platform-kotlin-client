package quantum.kpfu.ru.kotlinclient.rest

import io.reactivex.Single
import quantum.kpfu.ru.kotlinclient.QuantumProgram
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface QuantumPlatformApi {

    @Headers(
            "Accept: application/json",
            "X-Api-Key: &<C]E3z8L~yjw&D", //буэ
            "X-User-Id: quantum"
    )
    @POST("execute_program")
    fun executeProram(@Body program: QuantumProgram): Single<List<Result>>
}