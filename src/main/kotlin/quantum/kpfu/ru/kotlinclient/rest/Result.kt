package quantum.kpfu.ru.kotlinclient.rest

data class Result constructor(
        val logicalQubitAddressFromClient: LogicalQubitAddressFromClient,
        val value: Int
)