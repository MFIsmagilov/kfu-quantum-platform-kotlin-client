package quantum.kpfu.ru.kotlinclient.rest

import com.google.gson.annotations.SerializedName

data class LogicalQubitAddressFromClient(
        @SerializedName("logicalQubitAddress")
        private val mLogicalQubitAddress: Int
)