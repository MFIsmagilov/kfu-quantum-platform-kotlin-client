package quantum.kpfu.ru.kotlinclient.operators

interface Operator {
    fun toJson(): String
}