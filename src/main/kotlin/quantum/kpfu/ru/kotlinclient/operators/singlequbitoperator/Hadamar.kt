package quantum.kpfu.ru.kotlinclient.operators.singlequbitoperator

import quantum.kpfu.ru.kotlinclient.operators.AbstractOperator


class Hadamar(qubit: Int) : AbstractOperator(listOf(qubit)) {

    override val operator = "Hadamard"

    constructor(qubit: () -> Int) : this(qubit())

    companion object {
        fun use(qubits: Iterable<Int>): Array<Hadamar> {
            return qubits.map { Hadamar { it } }.toTypedArray()
        }
    }
}