package quantum.kpfu.ru.kotlinclient.operators.singlequbitoperator

import quantum.kpfu.ru.kotlinclient.operators.AbstractOperator

class S(qubit: Int) : AbstractOperator(listOf(qubit)) {
    override val operator = "S"

    constructor(qubit: () -> Int) : this(qubit())

    companion object {
        fun use(qubits: Iterable<Int>): Array<S> {
            return qubits.map { S { it } }.toTypedArray()
        }
    }
}