package quantum.kpfu.ru.kotlinclient.operators

import com.google.gson.Gson

abstract class AbstractOperator(val qubits: List<Int>) : Operator {

    abstract val operator: String

    override fun toJson(): String {
        return Gson().toJson(this)
    }
}