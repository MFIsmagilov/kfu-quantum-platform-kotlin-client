package quantum.kpfu.ru.kotlinclient

import com.google.gson.Gson
import io.reactivex.Single
import quantum.kpfu.ru.kotlinclient.operators.AbstractOperator
import quantum.kpfu.ru.kotlinclient.rest.Rest
import quantum.kpfu.ru.kotlinclient.rest.Result

class QuantumProgram(val countQubits: Int, val circuit: List<AbstractOperator>) {


    fun build(): Single<List<Result>> {
        println(Gson().toJson(this))
        return Rest.api.executeProram(this)
    }

    class Builder {

        private var countQubits: Int = 0
        lateinit var operators: List<AbstractOperator>

        fun setCountQubits(count: Int): Builder {
            countQubits = count
            return this
        }

        fun setOperators(vararg operators: AbstractOperator): Builder {
            this.operators = operators.toList()
            return this
        }

        fun build(): Single<List<Result>> {
            return QuantumProgram(countQubits, operators).build()
        }
    }
}
